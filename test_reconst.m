clc;
clear all;
close all;

%% Initializing the directories
global ROOTDIR
% Defining the root directory
ROOTDIR = fullfile(getenv('HOME'), 'Masters');
% setPaths is in recUtils
addpath(fullfile(ROOTDIR, 'recUtils'));
% Adding all the other repositories to the path
setPaths; % edit this function accordingly
% Folder where the phantom is saved
DATADIR = fullfile(ROOTDIR, 'TestData');
% Folder where the projection matrices are saved
MPDIR = fullfile(DATADIR, 'projMatrices');

%% Creating the phantom data and the sinogram
FACTOR = 1;
critPars.lambda = 0.1;
critPars.delta = 5e-03;
critPars.penalType = 'PenalGradObj_L2';
critPars.coordType = 'cyl';
critPars.imagPenal = false;
[fant, sino] = genData(DATADIR, FACTOR, ['phant_', num2str(FACTOR)]);
[crit, critPars] = genCrit(fant, sino, DATADIR, MPDIR, critPars);
prec = Precond_Factory.create('DiagF', 'crit', crit);

%% Solvers parameters
TOL = 1e-8;
PROJ_TOL = 1e-11;
MAX_ITER = 1e3;
PROJ_MAX_ITER = 2e5;
MAX_PROJ = 2e4;
MAX_EVAL = 1e3;
PROJ_MAX_EVAL = 2e5;
VERBOSE = 2;
PROJ_VERBOSE = 0;
MAX_RT = 15 * 60;
PROJ_MAX_RT = 5 * 60;
A_FEAS_TOL = 1e-15;

import model.ProjRecModel;
import model.ProjModel;

reconstNames = {'Bb', 'Cflash'};
projNames = {'Tmp2', 'Tmp2', 'Bb', 'Bcflash', 'Lbfgsb'};

recDefault = struct('aOptTol', TOL, 'maxIter', MAX_ITER, ...
    'maxEval', MAX_EVAL, 'maxProj', MAX_PROJ, 'verbose', VERBOSE, ...
    'maxRT', MAX_RT, 'aFeasTol', A_FEAS_TOL);
projDefault = struct('aOptTol', PROJ_TOL, 'maxIter', PROJ_MAX_ITER, ...
    'maxEval', PROJ_MAX_EVAL, 'verbose', PROJ_VERBOSE, ...
    'maxRT', PROJ_MAX_RT, 'aFeasTol', A_FEAS_TOL);

% Initialize every solvers options to default and then add custom params
recOpts.Bb = recDefault;
recOpts.Bb.bbType = 'abbss';
recOpts.Bb.memory = 3;
recOpts.Cflash = recDefault;
recOpts.Cflash.backtracking = true;
recOpts.Cflash.eqTol = PROJ_TOL;
recOpts.Cflash.cgTol = 1e-1;
projOpts.Bb = projDefault;
projOpts.Bb.bbType = 'abbss';
projOpts.Tmp2 = projDefault;
projOpts.Bcflash = projDefault;
projOpts.Bcflash.backtracking = true;
projOpts.Bcflash.cgTol = 1e-8;
projOpts.Lbfgsb = projDefault;

% Test Tmp for times using different solvers
tmpMethods = {'pcg', 'minres'};

% Save the stats structs inside this struct
% Keep track of factor and general optimization parameters
data.FACTOR = FACTOR;
data.recOpts = recDefault;
data.projOpts = projDefault;
data.infoHeader = {'pgNorm', 'solveTime', 'fx', 'nrmViol', 'nObjFunc', ...
    'nGrad', 'nHess', 'iter', 'nProj', 'nEqProj', 'iStop'};

% Cleaning up the diary
diaryName = ['./results/fact_', num2str(FACTOR), ...
    'bb_cflash-tmp2_bb_bcflash_lbfgsb-l2-obj.txt'];
fid = fopen(diaryName, 'w');
fclose(fid);
diary(diaryName);

for recName = reconstNames
    
    % Current reconstruction solver's name
    recSolverType = ['solvers.', recName{1}, 'Solver'];
    tmpCounter = 1;
    
    for projName = projNames
        fprintf('\n\n\n');
        if strcmp(projName{1}, 'Tmp2')
            projOpts.(projName{1}).method = tmpMethods{tmpCounter};
            tmpCounter = tmpCounter + 1;
        end
        
        % Current projection solver's name
        projSolverType = ['solvers.', projName{1}, 'Solver'];
        
        % Building the projection model
        projModel = ProjModel(prec, crit.J{2}.GeoS);
        % Creating the projection solver
        projSolver = eval([projSolverType, ...
            '(projModel, projOpts.(projName{1}))']);
        
        % Building the reconstruction model
        recModel = ProjRecModel(crit, prec, sino, crit.J{2}.GeoS, ...
            projSolver);
        
        % Creating the solver
        recSolver = eval([recSolverType, ...
            '(recModel, recOpts.(recName{1}))']);
        
        % Solving the problem
        recSolver.solve();
        
        % Plotting the figure
        if strcmp(projName{1}, 'Tmp2')
            figName = [recName{1}, '_', projName{1}, '_', ...
                projOpts.(projName{1}).method];
            pName = [projName{1}, projOpts.(projName{1}).method];
        else
            figName = [recName{1}, '_', projName{1}];
            pName = projName{1};
        end
        
        srie = genSerie(recSolver.x, crit.J{2}.GeoS, sino, prec, ...
            false, figName);
        
        %% Save run statistics
        if strcmp(recName{1}, 'Bb')
            nEqProj = 0;
        else
            nEqProj = recSolver.nEqProj;
        end
        nrmViol = norm(srie.Val(srie.Val < 0));
        stats = [recSolver.pgNorm, recSolver.solveTime, recSolver.fx, ...
            nrmViol, recSolver.nObjFunc, recSolver.nGrad, ...
            recSolver.nHess, recSolver.iter, recSolver.nProj, ...
            nEqProj, recSolver.iStop];
        data.(recName{1}).(pName).stats = stats;
        data.(recName{1}).(pName).x = srie.Val(:);
    end
    
end

diary off;

%% Saving data struct
[~, filename] = fetchNames(FACTOR, ...
    'COMP-bb-cflash-tmp-bb-bcflash-lbfgsb-l2-grad-obj', './results/');
save(filename, 'data');
