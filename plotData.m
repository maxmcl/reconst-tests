function plotData(data, filename)

cc = {'b-', 'g-', 'r-', 'c-', 'm-', 'k-', ...
    'b-.', 'g-.', 'r-.', 'c-.', 'm-.', 'k-.', ...
    'b--', 'g--', 'r--', 'c--', 'm--', 'k--'};

[~, m, p] = size(data.pMat);
header = data.infoHeader;
legFields = {'PCG', 'MINRES', 'LSQR', 'LSMR'};

[fold, filename, ~] = fileparts(filename);
filename = filename(isstrprop(filename, 'alphanum'));

for ii = 2 : m
    
    h = figure;
    set(h, 'Visible', 'off', 'DefaultAxesFontSize', 14);
    ind = 1;
    for jj = 1 : p
        if strcmp(header{ii}, 'pgNorm')
            loglog(data.pMat(:, 1, jj), data.pMat(:, ii, jj), cc{ind}, ...
                'LineWidth', 1.5);
        else
            semilogx(data.pMat(:, 1, jj), data.pMat(:, ii, jj), ...
                cc{ind}, 'LineWidth', 1.5);
        end
        hold on;
        ind = ind + 1;
    end
    
    hold off;
    xlabel(header{1});
    ylabel(header{ii});
    legend(legFields{:}, 'Location', 'BestOutside');
    
    % Save figure
    figName = [fold, '/', filename, header{ii}, '.eps'];
    set(h, 'Position', [404, 218, 560, 420]);
    set(gcf,'PaperPositionMode','auto')
    print(h, figName, '-depsc', '-r200');
end
end
