function data = test_lambda_study()

%% Initializing the directories
global ROOTDIR
% Defining the root directory
ROOTDIR = fullfile(getenv('HOME'), 'Masters');
% setPaths is in recUtils
addpath(fullfile(ROOTDIR, 'recUtils'));
% Adding all the other repositories to the path
setPaths; % edit this function accordingly
% Folder where the phantom is saved
DATADIR = fullfile(ROOTDIR, 'TestData');
% Folder where the projection matrices are saved
MPDIR = fullfile(DATADIR, 'projMatrices');

%% Solvers parameters
LAMBDAS = linspace(0.05, 0.1, 5);
LAMBDAS = LAMBDAS(2 : end -1);
% LAMBDAS = 10.^(-2:1:-1);
% LAMBDAS = kron([1.0, 5.0], LAMBDAS);
% LAMBDAS = LAMBDAS(5:end);
PENAL_TYPES = {'PenalGradObj_L2', 'PenalObj_L2'};
FACTOR = 1;

[~, filename] = fetchNames(FACTOR, ...
    'lambda-study', './lambda-study/');

MAX_ITER = 1e3;
PROJ_MAX_ITER = 2e4;
MAX_PROJ = 2e4;
MAX_EVAL = 1e3;
PROJ_MAX_EVAL = 2e4;
VERBOSE = 2;
PROJ_VERBOSE = 0;
MAX_RT = 15 * 60;
PROJ_MAX_RT = 5 * 60;
TOL = 1e-8;
PROJ_TOL = 1e-12;

recOpts = struct('maxIter', MAX_ITER, 'maxEval', MAX_EVAL, ...
    'maxProj', MAX_PROJ, 'verbose', VERBOSE, 'maxRT', MAX_RT, ...
    'aFeasTol', eps, 'aOptTol', TOL, 'eqTol', PROJ_TOL);
recOpts.backtracking = true;
recOpts.maxExtraIter = 15;
projOpts = struct('maxIter', PROJ_MAX_ITER, 'maxEval', ...
    PROJ_MAX_EVAL, 'verbose', PROJ_VERBOSE, 'maxRT', PROJ_MAX_RT, ...
    'aFeasTol', eps, 'aOptTol', PROJ_TOL);

% Other crit parameters
critPars.delta = 5e-03;
critPars.coordType = 'cyl';
critPars.imagPenal = false;

%% Save run statistics in this struct
data = struct;
data.FACTOR = FACTOR;
data.recOpts = recOpts;
data.projOpts = projOpts;
data.solvers = 'CflashSolver and Tmp2Solver';
data.infoHeader = {'x', 'lambda', 'penalty function type'};
data.xInfo = {};
data.infoHeader = {'lambda', 'pgNorm', 'solveTime', 'fx', 'nrmViol', ...
    'nObjFunc', 'nGrad', 'nHess', 'iter', 'nProj', 'iStop'};
mL = length(LAMBDAS);
mP = length(PENAL_TYPES);
mM = length(data.infoHeader);
data.pMat = nan(mL, mP, mM);

%% Solving everything
% Calling the models
import model.ProjModel;
import model.ProjRecModel;
import solvers.CflashSolver;
import solvers.Tmp2Solver;

% For reconstruction solvers
nL = 1;
for lambd = LAMBDAS
    nP = 1;
    for penalType = PENAL_TYPES
        
        fprintf('\n\n --- lambda = %3.1e, %s --- \n\n', lambd, ...
            penalType{1});
        
        % Choosing the lambda hyperparameter
        critPars.lambda = lambd;
        critPars.penalType = penalType{1};
        
        [fant, sino] = genData(DATADIR, FACTOR, ['phant_', ...
            num2str(FACTOR)]);
        [crit, ~] = genCrit(fant, sino, DATADIR, MPDIR, critPars);
        prec = Precond_Factory.create('DiagF', 'crit', crit);
        
        % Building the projection model & solver
        projModel = ProjModel(prec, crit.J{2}.GeoS);
        projSolver = solvers.Tmp2Solver(projModel, projOpts);
        
        % Building the reconstruction model & solver
        recModel = ProjRecModel(crit, prec, sino, ...
            crit.J{2}.GeoS, projSolver);
        recSolver = solvers.CflashSolver(recModel, recOpts);
        % Solving the problem
        recSolver.solve();
        
        %% Save run statistics
        srie = genSerie(recSolver.x, crit.J{2}.GeoS, sino, ...
            prec, true, '');
        nrmViol = norm(srie.Val(srie.Val < 0));
        stats = [lambd, recSolver.pgNorm, recSolver.solveTime, ...
            recSolver.fx, nrmViol, recSolver.nObjFunc, recSolver.nGrad, ...
            recSolver.nHess, recSolver.iter, recSolver.nProj, ...
            recSolver.iStop];
        data.pMat(nL, nP, :) = stats;
        
        data.xInfo{end + 1} = {srie.Val(:), lambd, penalType{1}};
        % Saving data struct -- preemptively in case MATLAB crashes
        save(filename, 'data');
        
        if log10(lambd) < 0
            fname = sprintf('lambdaN%3.1e%s', lambd, ...
                penalType{1});
        else
            fname = sprintf('lambda%3.1e%s', lambd, ...
                penalType{1});
        end
        
        fname = ['./lambda-study/fact', num2str(data.FACTOR), ...
            fname(isstrprop(fname, 'alphanum')), '.eps'];
        h = gcf();
        caxis([0, 0.35]);
        print(h, '-depsc', '-r200', fname);
        close();
        
        nP = nP + 1;
    end
    nL = nL + 1;
end
end