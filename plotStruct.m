function plotStruct(data)
% Assuming that each data.entry contains a matrix of same size.
% Increase markers if needed...
cc = {'b-', 'g-', 'r-', 'c-', 'm-', 'k-', ...
    'b-.', 'g-.', 'r-.', 'c-.', 'm-.', 'k-.', ...
    'b--', 'g--', 'r--', 'c--', 'm--', 'k--'};

% Get reconstruction problem solver
for recField = fields(data)'
    % Exclude FACTOR, recOpts & projOpts
    if ~strcmp(recField, 'FACTOR') && ~strcmp(recField, 'recOpts') && ...
            ~strcmp(recField, 'projOpts')
        
        % Parsing all the projection solvers for a given rec solver
        for sub = {'proj', 'rec'}
            
            % Probing for the header
            projFields = fields(data.(recField{1}));
            header = data.(recField{1}).(projFields{1}).(sub{1}). ...
                infoHeader;
            
            for varInd = 2 : size(header, 2)
                % Running over all columns seperately
                figName = [recField{1}, sub{1}, header{varInd}];
                figName = ['runData/fact', num2str(data.FACTOR), ...
                    figName(isstrprop(figName, 'alpha')), '.eps'];
                
                h = figure;
                set(h, 'Visible', 'off');
                projInd = 1;
                % Get projection problem solver
                for projField = projFields';
                    % Handling rec & proj seperately
                    tempData = data.(recField{1}).(projField{1}).(sub{1});
                    % Handling each column seperatly, 1st is the x, others
                    % are ys
                    loglog(tempData.info(:, 1), ...
                        tempData.info(:, varInd), cc{projInd});
                    hold on;
                    projInd = projInd + 1;
                end
                
                hold off;
                xlabel(tempData.infoHeader{1});
                ylabel(tempData.infoHeader{varInd});
%                 axis tight;
                
                clear tempData temp;
                
                legend(projFields{:}, 'Location', 'BestOutside');
                % Save figure
                print(h, '-depsc', '-r200', figName);
                close(h);
            end
        end
    end
end
end