function data = test_cflash()

%% Checking for missing arguments
PROJ_TOL = 1e-12;
TOLS = 10.^(-4:-1:-10);
FACTOR = 4;
cflashMethods = {'pcg', 'minres', 'lsqr', 'lsmr'};

%% Initializing everything
[recOpts, projOpts, prec, crit, sino, data] = init(FACTOR);

%% Solving everything
data.infoHeader = {'tol', 'pgNorm', 'solveTime', 'nObjFunc', 'nGrad', ...
    'nHess', 'nProj', 'fx', 'iterCg', 'iter', 'nEqProj'};
data.pMat = zeros(length(TOLS), length(data.infoHeader), ...
    length(cflashMethods));
% Calling the models
import model.ProjModel;
import model.ProjRecModel;
import solvers.CflashSolver;
import solvers.Tmp2Solver;

recOpts.cgTol = 1e-2;
recOpts.backtracking = true;
projOpts.aOptTol = PROJ_TOL;
recOpts.eqTol = PROJ_TOL;

% Iterating over various reconstruction tolerances ...
tolInd = 1;
for tol = TOLS
    
    methInd = 1;
    for tempMeth = cflashMethods
        
        meth = tempMeth{1};
        % Setting reconstruction problem tolerance
        recOpts.aOptTol = tol;
        recOpts.method = meth;
        
        % Building the projection model
        projModel = ProjModel(prec, crit.J{2}.GeoS);
        % Creating the projection solver
        projSolver = Tmp2Solver(projModel, projOpts);
        
        % Building the reconstruction model
        recModel = ProjRecModel(crit, prec, sino, ...
            crit.J{2}.GeoS, projSolver);
        % Creating the solver
        solver = CflashSolver(recModel, recOpts);
        % Solving the problem
        solver.solve();
        
        % Plotting the figure
        figName = ['Cflash_Tmp2_', recOpts.method];
        
        srie = genSerie(solver.x, crit.J{2}.GeoS, sino, ...
            prec, true, figName);
        
        %% Collecting statistics
        temp = [tol, solver.pgNorm, solver.solveTime, solver.nObjFunc, ...
            solver.nGrad, solver.nHess, solver.nProj, solver.fx, ...
            solver.iterCg, solver.iter, solver.nEqProj];
        data.pMat(tolInd, :, methInd) = temp;
        
        methInd = methInd + 1;
    end
    tolInd = tolInd + 1;
end

%% Saving data struct
[~, filename] = fetchNames(FACTOR, 'cflash', './cflash-study/');
save(filename, 'data');

plotData(data, filename);
end

function [recOpts, projOpts, prec, crit, ...
    sino, data] = init(FACTOR)
%% Initializing the directories
global ROOTDIR
% Defining the root directory
ROOTDIR = fullfile(getenv('HOME'), 'Masters');
% setPaths is in recUtils
addpath(fullfile(ROOTDIR, 'recUtils'));
% Adding all the other repositories to the path
setPaths; % edit this function accordingly
% Folder where the phantom is saved
DATADIR = fullfile(ROOTDIR, 'TestData');
% Folder where the projection matrices are saved
MPDIR = fullfile(DATADIR, 'projMatrices');

%% Creating the phantom data and the sinogram
critPars.lambda = 0.1; % 1e-00;
critPars.delta = 5e-03;
critPars.penalType = 'PenalGradObj_L2';
critPars.coordType = 'cyl';
critPars.imagPenal = false;

[fant, sino] = genData(DATADIR, FACTOR, ['phant_', num2str(FACTOR)]);
[crit, ~] = genCrit(fant, sino, DATADIR, MPDIR, critPars);
prec = Precond_Factory.create('DiagF', 'crit', crit);

%% Solvers parameters
MAX_ITER = 1e3;
PROJ_MAX_ITER = 1e4;
MAX_PROJ = 1e4;
MAX_EVAL = 1e3;
PROJ_MAX_EVAL = 1e4;
VERBOSE = 2;
PROJ_VERBOSE = 0;
MAX_RT = 6 * 60;
PROJ_MAX_RT = 2 * 60;

recDefault = struct('maxIter', MAX_ITER, 'maxEval', MAX_EVAL, ...
    'maxProj', MAX_PROJ, 'verbose', VERBOSE, 'maxRT', MAX_RT, ...
    'aFeasTol', eps);
projDefault = struct('maxIter', PROJ_MAX_ITER, 'maxEval', ...
    PROJ_MAX_EVAL, 'verbose', PROJ_VERBOSE, 'maxRT', PROJ_MAX_RT, ...
    'aFeasTol', eps);

% Initialize every solvers options to default and then add custom params
recOpts = recDefault;
recOpts.backtracking = true;

projOpts = projDefault;

% Save run statistics in this struct
data = struct;
data.FACTOR = FACTOR;
data.recOpts = recDefault;
data.projOpts = projDefault;
end