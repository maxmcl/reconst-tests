function [outLatex] = printReconst2Latex(data)
%% Print data stored in struct to LaTeX format for Bcflash & IPOPT

% Looking for fields that contain either 'Cflash' or 'Bb'
temp = fields(data);
ind = ~cellfun('isempty', regexp(temp, 'Cflash')) | ...
    ~cellfun('isempty', regexp(temp, 'Bb'));
recSolvers = temp(ind)';

fid = fopen('./results/output.txt', 'w+');
if fid == -1
    error('Can''t open file.');
end

% Setting up
nM = length(data.infoHeader);
nS = length(recSolvers);
% Projection solvers should be the same for each rec solver
projSolvers = fields(data.(recSolvers{1}))';
nP = length(projSolvers);
outLatex = cell(nS * nP, 1);

% Headers
% < rec solver | proj solver | header >
HDR_FRMT = [repmat('%s & ', 1, 1 + nM), '%s \\\\ \n'];
BDY_FRMT = ['\\multirow{ ', sprintf('%d}', nP), ...
    '{*}{%s}%s& %10s & \\num{%5.1e} & \\num{%5.1e} & \\num{%5.1e} & ', ...
    '\\num{%5.1e} & ', repmat('%5d & ', 1, 5), '%5s \\\\ \n'];
BDY_FRMT2 = ['%s& %10s & \\num{%5.1e} & \\num{%5.1e} & \\num{%5.1e} &', ...
    ' \\num{%5.1e} & ', repmat('%5d & ', 1, 5), '%5s \\\\ \n'];
PAD = 31;
    
% Add header to outLatex
outLatex{1} = sprintf(HDR_FRMT, 'Solver', 'Proj Solver', ...
    data.infoHeader{:});
% To output.txt
fprintf(fid, HDR_FRMT, 'Solver', 'Proj Solver', ...
    data.infoHeader{:});

cellRow = 2;
for tempSolver = recSolvers
    firstPass = true;
    recSolver = tempSolver{1};
    
    % Parse data
    for solver = projSolvers
        temp = data.(recSolver).(solver{1}).stats;
        msg = checkMsg(temp(end));
        if firstPass
            buf = repmat(' ', 1, 13 - length(recSolver));
            outLatex{cellRow} = sprintf(BDY_FRMT, recSolver, buf, ...
                solver{1}, temp(1 : end - 1), msg);
            fprintf(fid, BDY_FRMT, recSolver, buf, solver{1}, ...
                temp(1 : end - 1), msg);
            firstPass = false;
        else
            buf = repmat(' ', 1, PAD);
            outLatex{cellRow} = sprintf(BDY_FRMT2, buf, solver{1}, ...
                temp(1 : end - 1), msg);
            fprintf(fid, BDY_FRMT2, buf, solver{1}, ...
                temp(1 : end - 1), msg);
        end
        cellRow = cellRow + 1;
    end
    outLatex{cellRow} = sprintf('\\hline \n');
    fprintf(fid, '\\hline \n');
    cellRow = cellRow + 1;
end
end

function msg = checkMsg(iStop)
switch iStop
    case 1
        msg = '';
    case 2
        msg = 'feas.';
    case 3
        msg = 'eval.';
    case 4
        msg = 'iter.';
    case 5
        msg = 'proj.';
    case 6
        msg = 'cpu';
    case 7
        msg = 'search';
    case 8
        msg = 'inner fail.';
    case 9
        msg = 'unbounded';
    case 10
        msg = 'all at bound';
    case 11
        msg = 'dir. der.';
    case 12
        msg = 'step size';
    case 13
        msg = 'proj. fail.';
    case 14
        msg = 'xfail.';
    otherwise
        error('Can''t match string.');
end
end

