function data = test_tol_study(FACTOR, REC_NAMES, TOLS, PLOT_FLAG)

%% Checking for missing arguments
if nargin < 4
    PLOT_FLAG = false;
end
if nargin < 3
    TOLS = 10.^(-3:-1:-10);
end
if nargin < 2
    REC_NAMES = {'Bb', 'Cflash'};
end
if nargin < 1
    FACTOR = 4;
end

%% Initializing everything
[PROJ_NAMES, recOpts, projOpts, tmpMethods, prec, crit, sino] = ...
    init(FACTOR);

%% Save run statistics in this struct
data = struct;
data.FACTOR = FACTOR;
data.recOpts = recOpts;
data.projOpts = projOpts;
data.REC_NAMES = REC_NAMES;
data.PROJ_NAMES = PROJ_NAMES;
data.infoHeader = {'rec tol', 'pgNorm', 'solveTime', 'norm viol', 'fx', ...
    'avg proj pgNorm', 'avg proj solveTime', 'proj solveTime', 'nProj', ...
    'iter'};

np = length(TOLS);
nn = length(data.PROJ_NAMES);
nd = length(data.infoHeader);

data.projTolScheme = 'constant-tol-1e-12';
PROJ_TOL = 1e-12;
recOpts.Cflash.eqTol = PROJ_TOL;

%% Solving everything
% Calling the models
import model.ProjModel;
import model.ProjRecModel;

% For reconstruction solvers
for recName = REC_NAMES
    % Current reconstruction solver's name
    recSolverType = ['solvers.', recName{1}, 'Solver'];
    data.(recName{1}).pMat = nan(np, nn, nd);
    
    % For projection solvers
    nProj = 1;
    tmpCounter = 1;
    for projName = PROJ_NAMES
        % Current projection solver's name
        projSolverType = ['solvers.', projName{1}, 'Solver'];
        if strcmp(projName{1}, 'Tmp2')
            % Changing Krylov solver
            projOpts.(projName{1}).method = tmpMethods{tmpCounter};
            tmpCounter = tmpCounter + 1;
            pName = [projName{1}, projOpts.(projName{1}).method];
        else
            pName = projName{1};
        end
        % Init if its first pass
        data.(recName{1}).(pName) = {};
        
        % Iterating over various reconstruction tolerances
        nTol = 1;
        for tol = TOLS
            % Updating tolerances
            recOpts.(recName{1}).aOptTol = tol;
            projOpts.(projName{1}).aOptTol = 1e-2 * tol;
            
            % Building the projection model & solver
            projModel = ProjModel(prec, crit.J{2}.GeoS);
            projSolver = eval([projSolverType, ...
                '(projModel, projOpts.(projName{1}))']);
            
            % Building the reconstruction model & solver
            recModel = ProjRecModel(crit, prec, sino, ...
                crit.J{2}.GeoS, projSolver);
            recSolver = eval([recSolverType, ...
                '(recModel, recOpts.(recName{1}))']);
            % Solving the problem
            recSolver.solve();
            
            % Save run statistics
            srie = genSerie(recSolver.x, crit.J{2}.GeoS, sino, ...
                prec, false, '');
            nrmViol = norm(srie.Val(srie.Val < 0));
            % rec tol | pgNorm | solveTime | norm viol | fx | 
            % avg proj pgNorm | avg proj solveTime | proj solveTime |
            % nProj | iter
            data.(recName{1}).pMat(nTol, nProj, :) = [tol, ...
                recSolver.pgNorm, recSolver.solveTime, nrmViol, ...
                recSolver.fx, recSolver.stats.proj.info, ...
                recSolver.nProj, recSolver.iter];

            if strcmp(recName{1}, 'Cflash')
                data.(recName{1}).(pName){end + 1} = {tol, srie.Val(:), ...
                    recSolver.EXIT_MSG{recSolver.iStop}, ...
                    recSolver.stats.eqProj};
            else
                data.(recName{1}).(pName){end + 1} = {tol, srie.Val(:), ...
                    recSolver.EXIT_MSG{recSolver.iStop}};
            end
            
            nTol = nTol + 1;
        end
        nProj = nProj + 1;
    end
end

%% Saving data struct
[~, filename] = fetchNames(FACTOR, data.projTolScheme, './tolStudy/');
save(filename, 'data');

if PLOT_FLAG
    plotTolStudy(data);
end

end

function [projNames, recOpts, projOpts, tmpMethods, prec, crit, ...
    sino] = init(FACTOR)
%% Initializing the directories
global ROOTDIR
% Defining the root directory
ROOTDIR = fullfile(getenv('HOME'), 'Masters');
% setPaths is in recUtils
addpath(fullfile(ROOTDIR, 'recUtils'));
% Adding all the other repositories to the path
setPaths; % edit this function accordingly
% Folder where the phantom is saved
DATADIR = fullfile(ROOTDIR, 'TestData');
% Folder where the projection matrices are saved
MPDIR = fullfile(DATADIR, 'projMatrices');

%% Creating the phantom data and the sinogram
critPars.lambda = 0.5; % 1e-00;
critPars.delta = 5e-03;
critPars.penalType = 'PenalGradObj_L2';
critPars.coordType = 'cyl';
critPars.imagPenal = false;
[fant, sino] = genData(DATADIR, FACTOR, ['phant_', num2str(FACTOR)]);
[crit, ~] = genCrit(fant, sino, DATADIR, MPDIR, critPars);
prec = Precond_Factory.create('DiagF', 'crit', crit);

%% Solvers parameters
MAX_ITER = 1e3;
PROJ_MAX_ITER = 2.5e4;
MAX_PROJ = 2.5e4;
MAX_EVAL = 1e3;
PROJ_MAX_EVAL = 2.5e4;
VERBOSE = 2;
PROJ_VERBOSE = 0;
MAX_RT = 15 * 60;
PROJ_MAX_RT = 5 * 60;

% recNames is now an argin
projNames = {'Tmp2', 'Tmp2', 'Bb', 'Bcflash', 'Lbfgsb'};

recDefault = struct('maxIter', MAX_ITER, 'maxEval', MAX_EVAL, ...
    'maxProj', MAX_PROJ, 'verbose', VERBOSE, 'maxRT', MAX_RT, ...
    'aFeasTol', eps);
projDefault = struct('maxIter', PROJ_MAX_ITER, 'maxEval', ...
    PROJ_MAX_EVAL, 'verbose', PROJ_VERBOSE, 'maxRT', PROJ_MAX_RT, ...
    'aFeasTol', eps);

% Initialize every solvers options to default and then add custom params
recOpts.Bb = recDefault;
recOpts.Bb.bbType = 'ABBmin';
recOpts.Cflash = recDefault;
recOpts.Cflash.backtracking = true;
recOpts.Cflash.useBb = false;
projOpts.Bb = projDefault;
projOpts.Bb.bbType = 'ABBmin';
projOpts.Tmp2 = projDefault;
projOpts.Bcflash = projDefault;
projOpts.Bcflash.backtracking = true;
projOpts.Lbfgsb = projDefault;

% Test Tmp for times using different solvers
tmpMethods = {'pcg', 'minres'};
end