function plotTolStudy(data, filename)
% Assuming that each data.entry contains a matrix of same size.
% Increase markers if needed...
cc = {'b-o', 'g-d', 'r-x', 'c-s', 'm-v', 'k-+', ...
    'b-.^', 'g-.>', 'r-.<', 'c-.p', 'm-.h', 'k-.', ...
    'b--', 'g--', 'r--', 'c--', 'm--', 'k--'};

[~, filename, ~] = fileparts(filename);
filename = filename(isstrprop(filename, 'alphanum'));
header = data.infoHeader;
fold = ['./tolStudy/', filename];
mkdir(fold);

% Get reconstruction problem solver
for recField = fields(data)'
    % Exclude FACTOR, recOpts & projOpts
    if ~strcmp(recField, 'FACTOR') && ...
            ~strcmp(recField, 'recOpts') && ...
            ~strcmp(recField, 'projOpts') && ...
            ~strcmp(recField, 'REC_NAMES') && ...
            ~strcmp(recField, 'PROJ_NAMES') && ...
            ~strcmp(recField, 'infoHeader') && ...
            ~strcmp(recField, 'projTolScheme')
        
        % [ rec tol | solver | metric ]
        pMat = data.(recField{1}).pMat;
        tols = pMat(:, 1, 1); % Same for all columns (solvers)
        [~, ns, nm] = size(pMat);
        for indM = 2 : nm
            % Running over all columns seperately
            figName = [recField{1}, header{indM}];
            figName = [fold, '/fact', num2str(data.FACTOR), ...
                figName(isstrprop(figName, 'alphanum')), '.eps'];

            h = figure('DefaultAxesFontSize', 14, 'Visible', 'off');
            for indS = 1 : ns
                if strcmp(data.infoHeader{indM}, 'fx')
                    semilogx(tols, pMat(:, indS, indM), cc{indS}, ...
                        'Linewidth', 1.5);
                    hold on;
                else
                    loglog(tols, pMat(:, indS, indM), cc{indS}, ...
                        'Linewidth', 1.5); 
                    hold on;
                end
            end
            
            hold off;
            xlabel(header{1});
            ylabel(header{indM});
            legend(data.PROJ_NAMES, 'Location', 'BestOutside');
            % Save figure
            print(h, '-depsc', '-r200', figName);
        end
    end
end