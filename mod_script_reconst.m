%% Modification of the original 'scriptReconst.m' test file
clc;
close all;
clear all;

%% Initializing the directories
global ROOTDIR
% Defining the root directory
ROOTDIR = fullfile(getenv('HOME'), 'Masters');
% setPaths is in recUtils
addpath(fullfile(ROOTDIR, 'recUtils'));
% Adding all the other repositories to the path
setPaths; % edit this function accordingly
% Folder where the phantom is saved
DATADIR = fullfile(ROOTDIR, 'TestData');
% Folder where the projection matrices are saved
MPDIR = fullfile(DATADIR, 'projMatrices');

addpath(fullfile(ROOTDIR, 'scriptsreconstruction'));

%% Creating the phantom data and the sinogram
FACTOR = 1;
[fant, sino] = genData(DATADIR, FACTOR, ['phant_', num2str(FACTOR)]);
% Plotting the original object
% plot(fant.creationSerie(DATADIR))
% caxis([0, 0.35]);
% h = gcf();
% h.CurrentAxes.FontSize = 14;
% h.CurrentAxes.Title.String = '';
% h.Children(1).Label.String = 'X-R Att., cm-1, 70 keV';
% h.CurrentAxes.XTick = h.CurrentAxes.YTick;

%% Reconstruct using filtered back-projection in cylindrical & cartesian
% [recFBP_C, recFBP_P] = reconstFBP2D(fant, sino, DATADIR, MPDIR);
% % Plot the result
% recFBP_C.plot
% a = gca();
% a.Title.String = '';
% plot(recFBP_P.cvCart(fant.nbPixels, fant.nbPixels))
% a = gca();
% a.Title.String = '';

%% Create the 'criteria' objects for L-BFGS-B reconstruction
critPars.lambda = 1.5e1; % 1e-00;
critPars.delta = 5e-03;
critPars.penalType = 'PenalObj_L2';
critPars.coordType = 'cart';
critPars.imagPenal = false;
[crit_C, critPars] = genCrit(fant, sino, DATADIR, MPDIR, critPars);
% critPars.coordType = 'cyl';
% [crit_P, critPars] = genCrit(fant, sino, DATADIR, MPDIR, critPars);

%% Reconstruct using L-BFGS-B in cartesian coordinates
[recLBFGSB_C, recInfo_C] = reconstLBFGSB_C(sino, crit_C, [], ...
    [], []);
recLBFGSB_C.plot
caxis([0, 0.35]);
h = gcf();
h.CurrentAxes.FontSize = 14;
h.CurrentAxes.Title.String = '';
h.Children(1).Label.String = 'X-R Att., cm-1, 70 keV';
h.CurrentAxes.XTick = h.CurrentAxes.YTick;

%% Reconstruct using L-BFGS-B in cylindrical coordinates
% Pcnd = Precond_Factory.create('Identite');
% [recLBFGSB_P1, recInfo_P1] = reconstLBFGSB_P(sino, crit_P, Pcnd, ...
%     max_iter, epsilon, [], []);
% Pcnd.delete
% plot(recLBFGSB_P1.cvCart(fant.nbPixels, fant.nbPixels))
% a = gca();
% a.Title.String = '';

%% Cleaning
delete(crit_C.J{1}.MPrj)
% delete(crit_P.J{1}.MPrj)
h = get(0, 'Children');
for handle = h'
    figure(handle)
end